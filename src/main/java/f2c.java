import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class f2c extends HttpServlet {
    public double cf(String temp){
        try {
            return Double.parseDouble(temp) * 1.8f + 32.f;
        }
        catch (Exception e){
            return 0.0;
        }
    }
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType =
                "<!DOCTYPE HTML>\n";
        String title = "Farenheit";
        out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Celcius temperature: " +
                request.getParameter("temp") + "\n" +
                "  <P>Farenheit temperature: " +
                Double.toString(cf(request.getParameter("temp"))) +
                "</BODY></HTML>");
    }

}
